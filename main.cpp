#include <iostream>
#include <iostream>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/poll.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "httpparser/request.h"
#include "httpparser/httprequestparser.h"
#include "httpparser/response.h"
#include "httpparser/httpresponseparser.h"
#include "HttpCacheStore.h"
#include <netdb.h>

using namespace httpparser;

#define REQUEST_DEBUG 0

#define PROXY_IP "127.0.0.1"
#define PORT_LEFT 10000
#define PORT_RIGHT 60999
#define MAX_CONNECTIONS_NUMBER 500

#define READ_EOF 0
#define READ_PART 1
#define READ_FULL 2


int listening_sock;
struct pollfd polls[MAX_CONNECTIONS_NUMBER * 2 + 1];
char buffer[MAX_CONNECTIONS_NUMBER][BUFSIZ] = {0};  // 8192*500 = 4 мб
int bufferLen[MAX_CONNECTIONS_NUMBER] = {0}; // размер заполненной части буфера
pair<int, string>* clientAssignedToServer[MAX_CONNECTIONS_NUMBER] = {nullptr};
// (server_id | <client_id, url>) клиент - сервер
int clients_num = 0;
HttpCacheStore* cacheStore;
unordered_map<string,int> loadHandle;

void listeningSockStop();
void sigCatch(int sig);

void init(int argc, char **argv);
void arraysInit();

[[noreturn]] void mainLoopStart();

void poll_check(int poll_result);
int findFreeClientDesc();
int readRequest(int client_id);
void clientDetached(int client_id);

void newClientHandle();
void clientRequestsHandle(int client_id);
bool requestHandle(int client_id, Request *request, string *host);
bool findHostInRequest(int client_id, Request *request, string *host);
bool sendResponseCode(int client_id, int code);

bool newServerRequest(int client_id, Request* request, string host);
int findFreeServerDesc();
bool isOkResponse(char* response, int len);

void serverRequestsHandle(int serv_id);

int main(int argc, char *argv[]) {
    init(argc, argv);
    arraysInit();
    cacheStore = new HttpCacheStore;
    mainLoopStart();
    return 0;
}

void listeningSockStop() {
    shutdown(listening_sock, SHUT_RDWR);
    close(listening_sock);
    printf("\nProxy server listening socket closed\n");
    exit(0);
}

void sigCatch(int sig) {
    if (sig == SIGINT) listeningSockStop();
}

void init(int argc, char **argv){
    if (argc != 2) {
        printf("Usage: %s <proxy server port>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    const char *port_s = argv[1];
    uint16_t listening_port = strtoul(port_s, nullptr, 10);
    if (listening_port < PORT_LEFT || listening_port > PORT_RIGHT) {
        printf("Port must be in range [%d;%d]\n", PORT_LEFT, PORT_RIGHT);
        exit(EXIT_FAILURE);
    }

    printf("Proxy server (%s:%u) is starting...\n", PROXY_IP, listening_port);

    signal(SIGINT, sigCatch);

    if ((listening_sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket error");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in addr_listening{0};
    addr_listening.sin_family = AF_INET;
    addr_listening.sin_addr.s_addr = inet_addr(PROXY_IP);
    addr_listening.sin_port = htons(listening_port);

    if (bind(listening_sock, (struct sockaddr *) &addr_listening, sizeof(addr_listening)) == -1) {
        perror("bind error");
        exit(EXIT_FAILURE);
    }

    if (listen(listening_sock, MAX_CONNECTIONS_NUMBER) == -1) {
        perror("listen error");
        exit(EXIT_FAILURE);
    }

    printf("Proxy server (%s:%u) started successfully!\n",
           inet_ntoa(addr_listening.sin_addr), ntohs(addr_listening.sin_port));
}

void arraysInit(){
    for (int i = 0; i < MAX_CONNECTIONS_NUMBER * 2 + 1; ++i) // 0 - 1000
        polls[i].events = POLLIN;   // catch this kind of revents
    for (int i = 0; i < MAX_CONNECTIONS_NUMBER * 2; ++i)  // 0 - 990
        polls[i].fd = -1;
    polls[MAX_CONNECTIONS_NUMBER * 2].fd = listening_sock; // 1000

};

[[noreturn]] void mainLoopStart(){
    while(true){
        int respondClients = poll((struct pollfd *) &polls, MAX_CONNECTIONS_NUMBER * 2 + 1, -1);
        poll_check(respondClients);
        if (polls[MAX_CONNECTIONS_NUMBER * 2].revents == POLLIN) {
            newClientHandle();
        }
        for (int i = 0; i < MAX_CONNECTIONS_NUMBER; ++i){
            clientRequestsHandle(i);
        }
        for (int i = MAX_CONNECTIONS_NUMBER; i < MAX_CONNECTIONS_NUMBER * 2; ++i){
            serverRequestsHandle(i);
        }
    }
};

void poll_check(int poll_result) {
    if (poll_result == -1) {
        listeningSockStop();
        perror("poll failed");
        exit(1);
    }
    if (poll_result == 0) {
        listeningSockStop();
        perror("timeout");
        exit(1);
    }
}

int findFreeClientDesc(){
    for (int i = 0; i < MAX_CONNECTIONS_NUMBER; ++i) {
        if (polls[i].fd == -1)
            return i;
    }
    return -1;
};

int findFreeServerDesc(){
    for (int i = MAX_CONNECTIONS_NUMBER; i < MAX_CONNECTIONS_NUMBER*2; ++i) {
        if (polls[i].fd == -1)
            return i;
    }
    return -1;
};

void newClientHandle(){
    polls[MAX_CONNECTIONS_NUMBER * 2].revents = 0;
    if (clients_num < MAX_CONNECTIONS_NUMBER) {
        int id;
        if ((id = findFreeClientDesc()) != -1) {
            polls[id].fd = accept(listening_sock, nullptr, nullptr);
            printf("[%d] New client connected!\n", id);
            clients_num++;
        } else {
            printf("New client cannot be accepted, skipped\n");
        }
    }
}

void clientRequestsHandle(int client_id){
    if (polls[client_id].fd == -1 || polls[client_id].revents != POLLIN) return;
    polls[client_id].revents = 0;

    int status = readRequest(client_id);
    if (status == READ_PART) return;
    if (status == READ_EOF) {
        clientDetached(client_id);
        printf("[%d] client detached, client sent EOF\n", client_id);
        return;
    }
    // status == READ_FULL

    Request request;
    string host;
    bool isParsed = requestHandle(client_id, &request, &host);
    if (!isParsed){
        if (!sendResponseCode(client_id, 400)){
            clientDetached(client_id);
            printf("[%d] client detached, write to client error\n", client_id);
            return;
        }
        bufferLen[client_id] = 0;
        printf("[%d] Sent 400 to client, connection alive\n", client_id);
        return;
    }
    // isParsed == true

    if (!(request.versionMajor == 1 && request.versionMinor == 0)){
        sendResponseCode(client_id, 505);
        return;
    }
    if (request.method != "GET"){
        sendResponseCode(client_id, 405);
        return;
    }

    if (cacheStore->isStored(request.uri, client_id) == HttpCacheStore::FullStored){
        printf("[%d] client are getting data from cache\n", client_id);
        int num_read;
        char buf[BUFSIZ];
        while ((num_read = cacheStore->getChunk(request.uri, buf, client_id)) > 0) {
            if (write(polls[client_id].fd, buf, num_read) != num_read){
                clientDetached(client_id);
                fprintf(stderr, "[%d] client detached, client write error\n", client_id);
            }
        }
        clientDetached(client_id);
        fprintf(stderr, "[%d] client detached, client got all data\n", client_id);
    } else {
        newServerRequest(client_id, &request, host);
    }

    bufferLen[client_id] = 0;
}

int readRequest(int client_id){
    int num_read;
    char tempBuffer[BUFSIZ];
    if ((num_read = read(polls[client_id].fd, tempBuffer, BUFSIZ)) > 0) {
        if (REQUEST_DEBUG) cout << "---Пришла часть запроса---\n";
        for (int j = 0; j < num_read; ++j) {
            buffer[client_id][j + bufferLen[client_id]] = tempBuffer[j];
        }
        bufferLen[client_id] += num_read;
        if (REQUEST_DEBUG) cout << "tempBuffer: " << tempBuffer << "|" << endl;
        if (REQUEST_DEBUG) cout << "buffer[i]: ";
        if (REQUEST_DEBUG) for (int j = 0; j < bufferLen[client_id]; ++j) {
            printf("%c", buffer[client_id][j]);
        }
        if (REQUEST_DEBUG) cout << "|" << endl;
        string line;
        int countLines = 0;
        int lastLineLength = 0;
        int curLineLength = 0;
        for (int j = 0; j < bufferLen[client_id]; ++j) {
            if (buffer[client_id][j] != '\n') {
                curLineLength++;
            }
            else {
                countLines++;
                if (REQUEST_DEBUG) cout << curLineLength << endl;
                lastLineLength = curLineLength;
                curLineLength = 0;
            }
        }
        if (REQUEST_DEBUG) cout << "---Длина ласт строки [" << lastLineLength << "]---" << endl;
        if (REQUEST_DEBUG) cout << "---Конец части запроса длины [" << countLines << "]---" << endl;
        if (countLines >= 2){
            if (lastLineLength == 1){
                return READ_FULL; // полностью
            }
        }
        return READ_PART; // частично
    } else {
        return READ_EOF; // Пришел ЕОF
    }
}

void clientDetached(int client_id){
    close(polls[client_id].fd);
    polls[client_id].fd = -1;
    clients_num--;
    bufferLen[client_id] = 0;
}

bool requestHandle(int client_id, Request *request, string *host){
    HttpRequestParser parser;
    HttpRequestParser::ParseResult res = parser.parse(*request, buffer[client_id], buffer[client_id] + strlen(buffer[client_id]));
    if (res == HttpRequestParser::ParsingCompleted || res == HttpRequestParser::ParsingIncompleted){
        if (findHostInRequest(client_id, request, host))
            return true;
    }
    printf("[%d] Parsing error syntax\n", client_id);
    return false;
}

bool findHostInRequest(int client_id, Request *request, string *host){
    for (std::vector<Request::HeaderItem>::const_iterator it = request->headers.begin();
        it != request->headers.end(); ++it) {
        if (it->name == "Host") {
            // printf("[%d] Found host header: %s\n", client_id, it->value.c_str());
            *host = it->value;
            return true;
        }
    }
    printf("[%d] Host not found\n", client_id);
    return false;
}

bool sendResponseCode(int client_id, int code){
    char *query=(char*)malloc(100);
    if (code == 400) strcpy(query,"HTTP/1.1 400 Bad request\n");
    if (code == 405) strcpy(query,"HTTP/1.1 405 Method Not Allowed\n");
    if (code == 505) strcpy(query,"HTTP/1.1 505 HTTP Version Not Supported\n");
    if (code == 404) strcpy(query,"HTTP/1.1 404 Not Found\n");

    strcat(query, "Proxy-Connection: Keep-Alive\n\n");

    if (write(polls[client_id].fd, query, strlen(query)) == strlen(query)){
        free(query);
        printf("[%d] Sent code %d to client\n", client_id, code);
        return true;
    }
    free(query);
    return false;
}

bool newServerRequest(int client_id, Request* request, string host){
    struct sockaddr_in addr_target{0};
    addr_target.sin_family = AF_INET;

    if (host.find(':') == string::npos){
        addr_target.sin_port = htons(80);
    } else {
        string port = host.substr(host.find(':') + 1, host.length());
        printf("%s\n", port.c_str());
        host = host.substr(0, host.find(':'));
        addr_target.sin_port = htons(strtol(port.c_str(), nullptr, 10));
    }

    printf("[%d] client wants [%s] \n", client_id, request->uri.c_str());

    hostent* target_host;
    int serv_id;
    if ((serv_id = findFreeServerDesc()) != -1) {
        printf("[%d] for this client found free server [%d] \n", client_id, serv_id);
        if ((target_host = gethostbyname(host.c_str())) == nullptr) {
            printf("[%d] gethostbyname(%s) error\n", client_id, host.c_str());
            return false;
        }
    } else {
        printf("[%d] Free server desc not found, request skipped\n", client_id);
        return false;
    }
    // target_host != null
    polls[serv_id].fd = socket(AF_INET, SOCK_STREAM, 0);
    bool connect_successful = false;
    in_addr **addr_list = (struct in_addr **) target_host->h_addr_list;
    for (int j = 0; addr_list[j] != nullptr; j++) {
        addr_target.sin_addr = *addr_list[j];
        printf("[%d] trying to connect to (%s:%u) ", client_id,
               inet_ntoa(addr_target.sin_addr), ntohs(addr_target.sin_port));
        if (connect(polls[serv_id].fd, (struct sockaddr *) &addr_target, sizeof(addr_target)) == 0) {
            printf("[successfully]\n");
            connect_successful = true;
            break;
        }
        printf("[failed]\n");
    }
    if (!connect_successful){
        sendResponseCode(client_id, 404);
        return false;
    }

    clientAssignedToServer[serv_id-MAX_CONNECTIONS_NUMBER] = new pair<int, string>(make_pair(client_id, request->uri));

    if (write(polls[serv_id].fd, buffer[client_id], bufferLen[client_id]) != bufferLen[client_id]){
        close(polls[serv_id].fd);
        polls[serv_id].fd = -1;
        printf("[%d] server %d detached, write to server error\n", client_id, serv_id);
    }

    return true;
}

bool readResponse(int serv_id, int client_id, char* tempBuffer, int* num_read){
    *num_read = read(polls[serv_id].fd, tempBuffer, BUFSIZ);
    if (*num_read == 0){
        close(polls[serv_id].fd);
        polls[serv_id].fd = -1;
        free(clientAssignedToServer[serv_id-MAX_CONNECTIONS_NUMBER]);
        printf("[%d] server(%d) and client(%d) detached, server sent EOF\n", serv_id, serv_id, client_id);
        clientDetached(client_id); // mb
        return false;
    }
    if (*num_read == -1){
        close(polls[serv_id].fd);
        polls[serv_id].fd = -1;
        free(clientAssignedToServer[serv_id-MAX_CONNECTIONS_NUMBER]);
        fprintf(stderr,"[%d] server %d detached, server read error\n", serv_id, serv_id);
        return false;
    }
    return true;
}

void serverRequestsHandle(int serv_id){
    if (polls[serv_id].fd == -1 || polls[serv_id].revents != POLLIN) return;
    polls[serv_id].revents = 0;

    int client_id = clientAssignedToServer[serv_id-MAX_CONNECTIONS_NUMBER]->first;
    string url = clientAssignedToServer[serv_id-MAX_CONNECTIONS_NUMBER]->second;

    int num_read = 0;
    char tempBuffer[BUFSIZ];

    bool isRead = readResponse(serv_id, client_id, tempBuffer, &num_read);

    if (!isRead){
        if (cacheStore->isStored(url, serv_id) == HttpCacheStore::PartStored){
            if (loadHandle.find(url)->second == serv_id){
                cacheStore->finishDownload(url, serv_id);
                loadHandle.erase(url);
            }
        }
        return;
    }

    if (polls[client_id].fd != -1) {
        if (write(polls[client_id].fd, tempBuffer, num_read) != num_read) {
            clientDetached(client_id);
            fprintf(stderr, "[%d] client %d detached, write to client error\n", serv_id, client_id);
        }
    }

    if (cacheStore->isStored(url, serv_id) == HttpCacheStore::NotFound){
        if (isOkResponse(tempBuffer, strlen(tempBuffer))){
            cacheStore->create(url, tempBuffer, num_read, serv_id);
            loadHandle.insert(make_pair(url, serv_id));
        }
    }
    if (cacheStore->isStored(url, serv_id) == HttpCacheStore::PartStored){
        if (loadHandle.find(url)->second == serv_id){
            cacheStore->add(url, tempBuffer, num_read, serv_id);
        }
    }
};

// HTTP/1.0 503 Service Unavailable
bool isOkResponse(char* response, int len){
    HttpResponseParser parser;
    Response resp;
    HttpResponseParser::ParseResult res = parser.parse(resp, response, response + len);
    if (res == HttpResponseParser::ParsingCompleted || res == HttpResponseParser::ParsingIncompleted){
       if (resp.statusCode == 200){
           return true;
       }
    }
    return false;
};
