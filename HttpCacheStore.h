#ifndef MULTITHREADEDPROXY_HTTPCACHESTORE_H
#define MULTITHREADEDPROXY_HTTPCACHESTORE_H

#include <string>
#include <map>
#include <unordered_map>
#include <vector>

#define DEBUG 0
using namespace std;
class HttpCacheStore{
public:
    enum CacheStatus {
        FullStored,
        PartStored,
        NotFound
    };
    CacheStatus isStored(const string uri, int who){
        if (DEBUG) printf("[%d] cache check [%s]", who, uri.c_str());
        Cache::iterator it = cache.find(uri);
        if (it == cache.end()) {
            if (DEBUG) printf(" [No]\n");
            return NotFound;
        }
        CacheStatus cacheStatus = it->second.first;
        if (cacheStatus == FullStored){
            if (DEBUG) printf(" [Yes]\n");
            return FullStored;
        }
        if (DEBUG) printf(" [Partially]\n");
        return PartStored;
    };
    string cur_uri = {0};
    int p;
    int getChunk(const string uri, char* buffer, int who){
        vector<pair<char*, size_t>> vec = cache.find(uri)->second.second;
        if (cur_uri == uri){
            size_t len;
            if (vec.size() <= p) len = 0;
            else len = vec[p].second;
            for (size_t i = 0; i < len; ++i) {
                buffer[i] = vec[p].first[i];
            }
            if (DEBUG) printf("[%d] getting data [%s] from cache [part %d/%zu]\n", who, uri.c_str(), p, vec.size());
            p++;
            if (len == 0) p = 0;
            return len;
        } else {
            cur_uri = uri;
            p = 0;
            size_t len = vec[p].second;
            for (size_t i = 0; i < len; ++i) {
                buffer[i] = vec[p].first[i];
            }
            if (DEBUG) printf("[%d] getting data [%s] from cache [part %d/%zu]\n", who, uri.c_str(), p, vec.size());
            p++;
            return len;
        }
    };
    void add(const string uri, const char* buffer, size_t size, int who){
        char* part = (char *) calloc(size, sizeof(char));
        for (int i = 0; i < size; ++i) {
            part[i] = buffer[i];
        }
        Cache :: iterator it;
        it = cache.find(uri);
        it->second.second.emplace_back(part,size);
        if (DEBUG) printf("[%d] adding to cache data: [%s]\n", who, uri.c_str());
    };
    void create(const string uri, const char* buffer, size_t size, int who){
        char* part = (char *) calloc(size, sizeof(char));
        for (int i = 0; i < size; ++i) {
            part[i] = buffer[i];
        }
        vector<pair<char*,size_t>> parts;
        parts.emplace_back(part, size);
        cache.insert(make_pair(uri, make_pair(PartStored, parts)));
        if (DEBUG) printf("[%d] cache data created: [%s]\n", who, uri.c_str());
    };
    void finishDownload(const string uri, int who){
        if (isStored(uri ,who) == PartStored){
            Cache::iterator it = cache.find(uri);
            it->second.first = FullStored;
            if (DEBUG) printf("[%d] finished downloading [%s] in cache\n", who, uri.c_str());
        }
    }

private:
    typedef unordered_map<string, pair<CacheStatus, vector<pair<char*, size_t>>>> Cache;
    Cache cache;
    /*
     *  |lib.ru/text1.html, [FullStored, ("Hello world this is...", 8192)  |
     *  |                                ("summarizing all of...",  45  )] |
     *  |lib.ru/text2.html, [PartStored, ("Some text...",           8192)  |
     *  |                                ("Some more text...",      56  )] |
     *
     * */
};


#endif //MULTITHREADEDPROXY_HTTPCACHESTORE_H
